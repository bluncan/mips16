----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/25/2020 04:56:20 PM
-- Design Name: 
-- Module Name: instruction_fetch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity instruction_fetch is
  Port (jump: in std_logic;
        jump_address: in std_logic_vector(15 downto 0);
        jump_register_address: in std_logic_vector(15 downto 0);
        jumpR: in std_logic;
        pcsrc: in std_logic;
        branch_address: in std_logic_vector(15 downto 0);
        en: in std_logic;
        rst: in std_logic;
        clk: in std_logic;
        
        pc_out: out std_logic_vector(15 downto 0);
        instruction_out: out std_logic_vector(15 downto 0)
   );
end instruction_fetch;
architecture Behavioral of instruction_fetch is

type rom_memory is array(0 to 31) of std_logic_vector(15 downto 0);
signal rom : rom_memory := (b"001_000_001_0000101", -- 0x2085  addi $1, $0, 5 -- init reg1 cu val 5
                            b"001_000_101_0000000", -- 0x2280  addi $5, $0, 0 -- init reg5 cu val 0
                            b"001_000_010_0000001", -- 0x2101  addi $2, $0, 1 -- init reg2 cu val 1
                            b"001_000_011_0000001", -- 0x2181  addi $3, $0, 1 -- init reg 3 cu val 1
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"011_000_010_0010100", -- 0x6114  sw $2, 20($0) -- stocheaza in mem la addr 20 + $0 val din reg 2
                            b"011_000_010_0010101", -- 0x6115  sw $2 21($0)  -- stocheaza in mem la addr 21 + $0 val din reg 2
                            b"000_011_010_100_0_000", -- 0x0d40  add $4, $3, $2 -- aduna in $4 reg $3 si $2
                            b"000_011_000_010_0_000", -- 0x0c20  add $2, $3, $0 -- pune in $2 val din $3
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"000_100_000_011_0_000", -- 0x1030  add $3, $4, $0 -- pune in $3 val din $4
                            b"011_101_100_0010110",   -- 0x7616  sw  $4. 22($5) -- stocheaza la adr 22 + $5 val din $4
                            b"001_101_101_0000001",   -- 0x3681  addi $5, $5, 1 -- adauga in $5 val 1
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"100_001_101_0000101",   -- 0x8685  beq $5, $1, 5 -- daca $5 == %1 sare peste 5 instr
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"000_000_000_000_0_010", --- 0x0002 sll $0, $0, 0 -- NoOp
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"111_000_000_0000111",   -- 0xe007  j 7           -- sare la instr cu nr 7 ( a8a din program )
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"010_000_001_0011010",   -- 0x409a  lw $1, 26($0) -- incarca in $1 val din mem de la adresa 26 + $0
                            b"010_0000100011010",     -- 0x411a  lw $2, 26($0) -- incarca in $2 val din mem de la adresa 26 + $0
                            b"000_000_000_000_0_010", -- 0x0002 sll $0, $0, 0 -- NoOp
                            b"001_001_001_0000000",   -- 0x2480  addi $1, $1, 0 -- aduna 0 la registrul 1 (doar ca sa pot vedea continutul registrului pe ssd)
                            others => X"0000");
		-- programul salveaza in memorie incepand de la adresa 20 primi 7 termeni ai sirului fibonacci si incarca termenul 7 in registrul 1 si 2
							
							
signal pc_q: std_logic_vector(15 downto 0);
signal mux2_q: std_logic_vector(15 downto 0);
signal mux1_q: std_logic_vector(15 downto 0);
signal mux3_q: std_logic_vector(15 downto 0);

begin

pc: process(clk)
begin
    if rising_edge(clk) then
        if rst = '1' then
            pc_q <= X"0000";
        elsif en = '1' then
            pc_q <= mux3_q;
        end if;
    end if;
end process;

mux1_q <= pc_q + 1 when pcsrc = '0' else branch_address;
mux2_q <= mux1_q when jump = '0' else jump_address;
mux3_q <= mux2_q when jumpR = '0' else jump_register_address;

pc_out <= pc_q + 1;
instruction_out <= rom(conv_integer(pc_q(4 downto 0)));

end Behavioral;
