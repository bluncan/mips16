----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/04/2020 02:16:20 PM
-- Design Name: 
-- Module Name: ssd - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ssd is
    Port (
    clk: in std_logic;
    digit0: in std_logic_vector(3 downto 0);
    digit1: in std_logic_vector(3 downto 0);
    digit2: in std_logic_vector(3 downto 0);
    digit3: in std_logic_vector(3 downto 0);
    an: out std_logic_vector(3 downto 0);
    cat: out std_logic_vector(6 downto 0)
     );
end ssd;

architecture Behavioral of ssd is
    signal counter: std_logic_vector(15 downto 0) := X"0000";
    signal mux1_out: std_logic_vector(3 downto 0) := X"0";
begin
    
    counter_process: process(clk)
    begin
        if rising_edge(clk) then
            counter <= counter + 1;
        end if;
    end process;
    
    mux1_process: process(counter(15 downto 14), digit0, digit1, digit2, digit3)
    begin
        case counter(15 downto 14) is
            when "00" => mux1_out <= digit0;
            when "01" => mux1_out <= digit1;
            when "10" => mux1_out <= digit2;
            when "11" => mux1_out <= digit3;
        end case;
    end process;
    
    mux2_process: process(counter(15 downto 14))
    begin
        case counter(15 downto 14) is
            when "00" => an <= "1110";
            when "01" => an <= "1101";
            when "10" => an <= "1011";
            when "11" => an <= "0111";
        end case;
    end process;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
    hex_to_7_segments_process: process(mux1_out)
    begin
        case mux1_out is
            when X"0" => cat <= "1000000";
            when X"1" => cat <= "1111001";
            when X"2" => cat <= "0100100";
            when X"3" => cat <= "0110000";
            when X"4" => cat <= "0011001";
            when X"5" => cat <= "0010010";
            when X"6" => cat <= "0000010";
            when X"7" => cat <= "1111000";
            when X"8" => cat <= "0000000";
            when X"9" => cat <= "0010000";
            when X"A" => cat <= "0001000";
            when X"B" => cat <= "0000011";
            when X"C" => cat <= "1000110";
            when X"D" => cat <= "0100001";
            when X"E" => cat <= "0000110";
            when X"F" => cat <= "0001110";
            --when others => cat <= "1111111";
       end case;
    end process;
    
end Behavioral;
