----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/08/2020 05:52:11 PM
-- Design Name: 
-- Module Name: mem - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values


-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mem is
    Port( clk: in std_logic;
          en: in std_logic;
          MemWrite: in std_logic;
          ALUResIn: in std_logic_vector(15 downto 0);
          WD: in std_logic_vector(15 downto 0);
          
          ALUResOut: out std_logic_vector(15 downto 0);
          MemData: out std_logic_vector(15 downto 0)
     );
end mem;

architecture Behavioral of mem is
type memory is array(0 to 31) of std_logic_vector(15 downto 0);
signal ram: memory;
signal address: std_logic_vector(4 downto 0);
begin

address <= ALUResIn(4 downto 0);
MemData <= ram(conv_integer(address));
ALUResOut <= ALUResIn;
    
    process (clk)
    begin
        if rising_edge(clk) then
            if en = '1' and MemWrite = '1' then
                ram(conv_integer(address)) <= WD;
            end if;
       end if;
    end process;

end Behavioral;
