----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/06/2020 02:47:27 PM
-- Design Name: 
-- Module Name: Lab3 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Lab7 is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0));
end Lab7;

architecture Behavioral of Lab7 is

component MPG 
     Port ( clk : in std_logic;
     btn : in std_logic;
     enable : out std_logic );
end component;

component ssd is
    Port (
    clk: in std_logic;
    digit0: in std_logic_vector(3 downto 0);
    digit1: in std_logic_vector(3 downto 0);
    digit2: in std_logic_vector(3 downto 0);
    digit3: in std_logic_vector(3 downto 0);
    an: out std_logic_vector(3 downto 0);
    cat: out std_logic_vector(6 downto 0)
    );
end component;

component instruction_fetch is
  Port (jump: in std_logic;
        jump_address: in std_logic_vector(15 downto 0);
        jump_register_address: in std_logic_vector(15 downto 0);
        jumpR: in std_logic;
        pcsrc: in std_logic;
        branch_address: in std_logic_vector(15 downto 0);
        en: in std_logic;
        rst: in std_logic;
        clk: in std_logic;
        
        pc_out: out std_logic_vector(15 downto 0);
        instruction_out: out std_logic_vector(15 downto 0)
   );
end component;

component main_control is
    Port( Instr: in std_logic_vector(2 downto 0);
          
          RegDst: out std_logic;
          ExtOp: out std_logic;
          ALUSrc: out std_logic;
          Branch: out std_logic;
          Jump: out std_logic;
          AluOp: out std_logic_vector(2 downto 0);
          MemWrite: out std_logic;
          MemToReg: out std_logic;
          RegWrite: out std_logic
    );
end component;

component instruction_decode is
 Port ( clk: in std_logic;
         enable: in std_logic;
         RegWrite: in std_logic;
         Instr: in std_logic_vector(12 downto 0);
         ExtOp: in std_logic;
         WD: in std_logic_vector(15 downto 0);
         WA: in std_logic_vector(2 downto 0);
         
         RD1: out std_logic_vector(15 downto 0);
         RD2: out std_logic_vector(15 downto 0);
         Ext_Imm: out std_logic_vector(15 downto 0);
         func: out std_logic_vector(2 downto 0);
         sa: out std_logic;
         rt: out std_logic_vector(2 downto 0);
         rd: out std_logic_vector(2 downto 0)
   );
end component;

component ex is
    Port( RD1: in std_logic_vector(15 downto 0);
          ALUSrc: in std_logic;
          RD2: in std_logic_vector(15 downto 0);
          Ext_Imm: in std_logic_vector(15 downto 0);
          sa: std_logic;
          func: in std_logic_vector(2 downto 0);
          AluOp: in std_logic_vector(2 downto 0);
          Pc_Plus: in std_logic_vector(15 downto 0);
          rt: in std_logic_vector(2 downto 0);
          rd: in std_logic_vector(2 downto 0);
          RegDst: in std_logic;
          
          Zero: out std_logic;
          ALUOut: out std_logic_vector(15 downto 0);
          BranchAddress: out std_logic_vector(15 downto 0);
          JumpR: out std_logic;
          rWA: out std_logic_vector(2 downto 0)
     );
end component;

component mem is
    Port( clk: in std_logic;
          en: in std_logic;
          MemWrite: in std_logic;
          ALUResIn: in std_logic_vector(15 downto 0);
          WD: in std_logic_vector(15 downto 0);
          
          ALUResOut: out std_logic_vector(15 downto 0);
          MemData: out std_logic_vector(15 downto 0)
     );
end component;

component reg is
  generic (width: natural);
  Port ( clk: in std_logic;
         enable: in std_logic;
         d: in std_logic_vector(width - 1 downto 0);
         q: out std_logic_vector(width -1 downto 0)
        );
end component;

signal En_mpg1: std_logic;
signal En_mpg0: std_logic;
signal Ssd_in: std_logic_vector(15 downto 0);

-- semnale if
signal Pc_out: std_logic_vector(15 downto 0);
signal Instruction: std_logic_vector(15 downto 0);
signal JumpR: std_logic;
signal BranchAddress: std_logic_vector(15 downto 0);
signal JumpAddress: std_logic_vector(15 downto 0);
signal PCSrc: std_logic;

-- semnale mc
signal RegDst: std_logic;
signal ExtOp: std_logic;
signal ALUSrc: std_logic;
signal Branch: std_logic;
signal Jump: std_logic;
signal AluOp: std_logic_vector(2 downto 0);
signal MemWrite: std_logic;
signal MemToReg: std_logic;
signal RegWrite: std_logic;

-- semname id
signal WD: std_logic_vector(15 downto 0);
signal RD1: std_logic_vector(15 downto 0);
signal RD2: std_logic_vector(15 downto 0);
signal Ext_Imm: std_logic_vector(15 downto 0);
signal func: std_logic_vector(2 downto 0);
signal sa: std_logic;
signal rd: std_logic_vector(2 downto 0);
signal rt: std_logic_vector(2 downto 0);
signal wa: std_logic_vector(2 downto 0);

-- semnale ex
signal Zero: std_logic;
signal ALUOut: std_logic_vector(15 downto 0);

--semnale mem
signal ALUResOut: std_logic_vector(15 downto 0);
signal MemData: std_logic_vector(15 downto 0);

--semnale registre pipeline
signal d_if_id: std_logic_vector(31 downto 0);
signal q_if_id: std_logic_vector(31 downto 0);
signal d_id_ex: std_logic_vector(82 downto 0);
signal q_id_ex: std_logic_vector(82 downto 0);
signal d_ex_mem: std_logic_vector(56 downto 0);
signal q_ex_mem: std_logic_vector(56 downto 0);
signal d_mem_wb: std_logic_vector(36 downto 0);
signal q_mem_wb: std_logic_vector(36 downto 0);

begin

mpg0: mpg port map(clk=>clk, btn=>btn(0), enable=>En_mpg0);
mpg1: mpg port map(clk=>clk, btn=>btn(1), enable=>En_mpg1);

ssd1: ssd port map(clk=>clk, digit0=>Ssd_in(3 downto 0), digit1=>Ssd_in(7 downto 4), digit2=>Ssd_in(11 downto 8), digit3=>Ssd_in(15 downto 12), 
                   an=>an, cat=>cat);

PCSrc <= q_ex_mem(53) and q_ex_mem(36);
instr_fetch: instruction_fetch port map(jump=>Jump, jump_address=>JumpAddress, jump_register_address=>q_ex_mem(35 downto 20), jumpR=>q_ex_mem(0), 
                                       pcsrc=>PCSrc, branch_address=>q_ex_mem(52 downto 37), en=>en_mpg0, rst=>en_mpg1, clk=>clk, 
                                       pc_out=>pc_out, instruction_out=>instruction);

mc: main_control port map(Instr=>q_if_id(31 downto 29), RegDst=>RegDst, ExtOp=>ExtOp, ALUSrc=>ALUSrc, Branch=>Branch, 
                          Jump=>Jump, ALuOp=>AluOp, MemWrite=>MemWrite, MemToReg=>MemToReg, RegWrite=>RegWrite);


id: instruction_decode port map(clk=>clk, enable=>En_mpg0, RegWrite=>q_mem_wb(35), Instr=>q_if_id(28 downto 16), ExtOp=>ExtOp,
                               WD=>WD, wa=>q_mem_wb(2 downto 0), RD1=>RD1, RD2=>RD2, Ext_Imm=>Ext_Imm, func=>func, sa=>sa, rt=>rt, rd=>rd);
                                
ex1: ex port map(RD1=>q_id_ex(57 downto 42), ALUSrc=>q_id_ex(75), RD2=>q_id_ex(41 downto 26), Ext_Imm=>q_id_ex(25 downto 10), sa=>q_id_ex(6), 
                 func=>q_id_ex(9 downto 7), ALUOp=>q_id_ex(78 downto 76), Pc_Plus=>q_id_ex(73 downto 58), rd=>q_id_ex(2 downto 0), 
                 rt=>q_id_ex(5 downto 3), RegDst=>q_id_ex(74), rWA=>wa,
                 Zero=>Zero, ALUOut=>ALUOut, BranchAddress=>BranchAddress, JumpR=>JumpR);
                 
mem1: mem port map(clk=>clk, en=>En_mpg0, MemWrite=>q_ex_mem(54), ALUResIn=>q_ex_mem(35 downto 20), WD=>q_ex_mem(19 downto 4), ALUResOut=>ALUResOut, MemData=>MemData);

JumpAddress <= q_if_id(15 downto 13) & q_if_id(28 downto 16);

mux: process(sw(8 downto 5), Instruction, pc_out, q_id_ex(57 downto 42), q_id_ex(41 downto 26), q_id_ex(25 downto 10), ALUOut, MemData, WD)
begin
    case sw(7 downto 5) is
        when "000" => ssd_in <= Instruction; 
        when "001" => ssd_in <= pc_out;
        when "010" => ssd_in <= q_id_ex(57 downto 42);
        when "011" => ssd_in <= q_id_ex(41 downto 26);
        when "100" => ssd_in <= q_id_ex(25 downto 10);
        when "101" => ssd_in <= ALUOut;
        when "110" => Ssd_in <= MemData;
        when "111" => Ssd_in <= WD;
    end case;
end process;

WD <= q_mem_wb(34 downto 19) when q_mem_wb(36) = '0' else q_mem_wb(18 downto 3);

d_if_id <= instruction & pc_out;
reg_if_id: reg generic map(width=>32) port map(clk=>clk, enable=>En_mpg0, d=>d_if_id, q=>q_if_id);
d_id_ex <= MemToReg & RegWrite & MemWrite & Branch & ALUOp & ALUSrc & RegDst & q_if_id(15 downto 0) & RD1 & RD2 & Ext_Imm & func & sa & rt & rd;
reg_id_ex: reg generic map(width=>83) port map(clk=>clk, enable=>En_mpg0, d=>d_id_ex, q=>q_id_ex);
d_ex_mem <= q_id_ex(82 downto 79) & BranchAddress & Zero & ALUOut & q_id_ex(41 downto 26) & wa & jumpR;
reg_ex_mem: reg generic map(width=>57) port map(clk=>clk, enable=>En_mpg0, d=>d_ex_mem, q=>q_ex_mem);
d_mem_wb <= q_ex_mem(56 downto 55) & ALUResOut & MemData & q_ex_mem(3 downto 1);
reg_mem_wb: reg generic map(width=>37) port map(clk=>clk, enable=>En_mpg0, d=>d_mem_wb, q=>q_mem_wb);

led(11 downto 0) <= ALUOp & q_id_ex(74) & ExtOp & q_id_ex(75) & q_ex_mem(53) & Jump & q_ex_mem(0) & q_ex_mem(54) & q_mem_wb(36) & q_mem_wb(35);

end Behavioral;