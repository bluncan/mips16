----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/01/2020 05:38:46 PM
-- Design Name: 
-- Module Name: main_control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main_control is
    Port( Instr: in std_logic_vector(2 downto 0);
          
          RegDst: out std_logic;
          ExtOp: out std_logic;
          ALUSrc: out std_logic;
          Branch: out std_logic;
          Jump: out std_logic;  
          AluOp: out std_logic_vector(2 downto 0);
          MemWrite: out std_logic;
          MemToReg: out std_logic;
          RegWrite: out std_logic
    );
end main_control;

architecture Behavioral of main_control is
begin
    process(Instr)
    begin
        RegDst <= '0';
        ExtOp <= '0';
        ALUSrc <= '0';
        Branch <= '0';
        Jump <= '0';
        AluOp <= "000";
        MemWrite <= '0';
        MemToReg <= '0';
        RegWrite <= '0';
        case Instr is
            when "000" => RegDst <= '1'; RegWrite <= '1'; ALUOp <= "000";
            when "001" => ExtOp <= '1'; ALUSrc <= '1'; RegWrite <= '1'; ALUOp <= "001";
            when "010" => ExtOp <= '1'; ALUSrc <= '1'; MemToReg <= '1'; RegWrite <= '1'; ALUOp <= "001";
            when "011" => ExtOp <= '1'; ALUSrc <= '1'; MemWrite <= '1'; ALUOp <= "001";
            when "100" => ExtOp <= '1'; Branch <= '1'; ALUOp <= "010";
            when "101" => RegDst <= '1'; ExtOp <= '1'; ALUSrc <= '1'; RegWrite <= '1'; ALUOp <= "011";
            when "110" => ALUSrc <= '1'; RegWrite <= '1'; ALUOp <= "100";
            when "111" => Jump <= '1'; AluOP <= "001";
        end case;
    end process;


end Behavioral;
