----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/29/2020 05:02:01 PM
-- Design Name: 
-- Module Name: reg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity reg is
  generic (width: natural);
  Port ( clk: in std_logic;
         enable: in std_logic;
         d: in std_logic_vector(width - 1 downto 0);
         q: out std_logic_vector(width -1 downto 0)
        );
end reg;

architecture Behavioral of reg is

begin
    process(clk)
    begin
        if rising_edge(clk) and enable = '1' then
            q <= d;
        end if;
    end process;

end Behavioral;
