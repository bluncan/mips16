----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/06/2020 02:47:27 PM
-- Design Name: 
-- Module Name: Lab3 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Lab7 is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0));
end Lab7;

architecture Behavioral of Lab7 is

component MPG 
     Port ( clk : in std_logic;
     btn : in std_logic;
     enable : out std_logic );
end component;

component ssd is
    Port (
    clk: in std_logic;
    digit0: in std_logic_vector(3 downto 0);
    digit1: in std_logic_vector(3 downto 0);
    digit2: in std_logic_vector(3 downto 0);
    digit3: in std_logic_vector(3 downto 0);
    an: out std_logic_vector(3 downto 0);
    cat: out std_logic_vector(6 downto 0)
    );
end component;

component instruction_fetch is
  Port (jump: in std_logic;
        jump_address: in std_logic_vector(15 downto 0);
        jump_register_address: in std_logic_vector(15 downto 0);
        jumpR: in std_logic;
        pcsrc: in std_logic;
        branch_address: in std_logic_vector(15 downto 0);
        en: in std_logic;
        rst: in std_logic;
        clk: in std_logic;
        
        pc_out: out std_logic_vector(15 downto 0);
        instruction_out: out std_logic_vector(15 downto 0)
   );
end component;

component main_control is
    Port( Instr: in std_logic_vector(2 downto 0);
          
          RegDst: out std_logic;
          ExtOp: out std_logic;
          ALUSrc: out std_logic;
          Branch: out std_logic;
          Jump: out std_logic;
          AluOp: out std_logic_vector(2 downto 0);
          MemWrite: out std_logic;
          MemToReg: out std_logic;
          RegWrite: out std_logic
    );
end component;

component instruction_decode is
  Port ( clk: in std_logic;
         enable: in std_logic;
         RegWrite: in std_logic;
         Instr: in std_logic_vector(12 downto 0);
         RegDst: in std_logic;
         ExtOp: in std_logic;
         WD: in std_logic_vector(15 downto 0);
         
         RD1: out std_logic_vector(15 downto 0);
         RD2: out std_logic_vector(15 downto 0);
         Ext_Imm: out std_logic_vector(15 downto 0);
         func: out std_logic_vector(2 downto 0);
         sa: out std_logic
   );
end component;

component ex is
    Port( RD1: in std_logic_vector(15 downto 0);
          ALUSrc: in std_logic;
          RD2: in std_logic_vector(15 downto 0);
          Ext_Imm: in std_logic_vector(15 downto 0);
          sa: std_logic;
          func: in std_logic_vector(2 downto 0);
          AluOp: in std_logic_vector(2 downto 0);
          Pc_Plus: in std_logic_vector(15 downto 0);
          
          Zero: out std_logic;
          ALUOut: out std_logic_vector(15 downto 0);
          BranchAddress: out std_logic_vector(15 downto 0);
          JumpR: out std_logic  
     );
end component;

component mem is
    Port( clk: in std_logic;
          en: in std_logic;
          MemWrite: in std_logic;
          ALUResIn: in std_logic_vector(15 downto 0);
          WD: in std_logic_vector(15 downto 0);
          
          ALUResOut: out std_logic_vector(15 downto 0);
          MemData: out std_logic_vector(15 downto 0)
     );
end component;

signal En_mpg1: std_logic;
signal En_mpg0: std_logic;
signal Ssd_in: std_logic_vector(15 downto 0);

-- semnale if
signal Pc_out: std_logic_vector(15 downto 0);
signal Instruction: std_logic_vector(15 downto 0);
signal JumpR: std_logic;
signal BranchAddress: std_logic_vector(15 downto 0);
signal JumpAddress: std_logic_vector(15 downto 0);
signal PCSrc: std_logic;

-- semnale mc
signal RegDst: std_logic;
signal ExtOp: std_logic;
signal ALUSrc: std_logic;
signal Branch: std_logic;
signal Jump: std_logic;
signal AluOp: std_logic_vector(2 downto 0);
signal MemWrite: std_logic;
signal MemToReg: std_logic;
signal RegWrite: std_logic;

-- semname id
signal WD: std_logic_vector(15 downto 0);
signal RD1: std_logic_vector(15 downto 0);
signal RD2: std_logic_vector(15 downto 0);
signal Ext_Imm: std_logic_vector(15 downto 0);
signal func: std_logic_vector(2 downto 0);
signal sa: std_logic;

-- semnale ex
signal Zero: std_logic;
signal ALUOut: std_logic_vector(15 downto 0);

--semnale mem
signal ALUResOut: std_logic_vector(15 downto 0);
signal MemData: std_logic_vector(15 downto 0);

begin

mpg0: mpg port map(clk=>clk, btn=>btn(0), enable=>En_mpg0);
mpg1: mpg port map(clk=>clk, btn=>btn(1), enable=>En_mpg1);

ssd1: ssd port map(clk=>clk, digit0=>Ssd_in(3 downto 0), digit1=>Ssd_in(7 downto 4), digit2=>Ssd_in(11 downto 8), digit3=>Ssd_in(15 downto 12), 
                   an=>an, cat=>cat);

PCSrc <= Branch and Zero;
instr_fetch: instruction_fetch port map(jump=>Jump, jump_address=>JumpAddress, jump_register_address=>RD1, jumpR=>JumpR, 
                                        pcsrc=>PCSrc, branch_address=>BranchAddress, en=>en_mpg0, rst=>en_mpg1, clk=>clk, 
                                        pc_out=>pc_out, instruction_out=>instruction);

mc: main_control port map(Instr=>instruction(15 downto 13), RegDst=>RegDst, ExtOp=>ExtOp, ALUSrc=>ALUSrc, Branch=>Branch, 
                          Jump=>Jump, ALuOp=>AluOp, MemWrite=>MemWrite, MemToReg=>MemToReg, RegWrite=>RegWrite);


id: instruction_decode port map(clk=>clk, enable=>En_mpg0, RegWrite=>RegWrite, Instr=>Instruction(12 downto 0), RegDst=>RegDst, ExtOp=>ExtOp,
                                WD=>WD, RD1=>RD1, RD2=>RD2, Ext_Imm=>Ext_Imm, func=>func, sa=>sa);
                                
ex1: ex port map(RD1=>RD1, ALUSrc=>ALUSrc, RD2=>RD2, Ext_Imm=>Ext_Imm, sa=>sa, func=>func, ALUOp=>ALUOp, Pc_Plus=>pc_out, 
                 Zero=>Zero, ALUOut=>ALUOut, BranchAddress=>BranchAddress, JumpR=>JumpR);
                 
mem1: mem port map(clk=>clk, en=>En_mpg0, MemWrite=>MemWrite, ALUResIn=>ALUOut, WD=>RD2, ALUResOut=>ALUResOut, MemData=>MemData);

JumpAddress <= pc_out(15 downto 13) & Instruction(12 downto 0);

mux: process(sw(7 downto 5), Instruction, pc_out, RD1, RD2, Ext_Imm, ALUOut, MemData, WD)
begin
    case sw(7 downto 5) is
        when "000" => ssd_in <= Instruction; 
        when "001" => ssd_in <= pc_out;
        when "010" => ssd_in <= RD1;
        when "011" => ssd_in <= RD2;
        when "100" => ssd_in <= Ext_Imm;
        when "101" => ssd_in <= ALUOut;
        when "110" => Ssd_in <= MemData;
        when "111" => Ssd_in <= WD;
    end case;
end process;

WD <= ALUResOut when MemToReg = '0' else MemData;

led(10 downto 0) <= ALUOp & RegDst & ExtOp & ALUSrc & Branch & Jump & MemWrite & MemToReg & RegWrite;
led(15) <= jumpR;

end Behavioral;