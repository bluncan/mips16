----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/11/2020 04:24:58 PM
-- Design Name: 
-- Module Name: RF - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity RF is
  Port (
         clk: in std_logic;
         enable: in std_logic;
         RA1: in std_logic_vector(2 downto 0);
         RA2: in std_logic_vector(2 downto 0);
         RegWrite: in std_logic;
         WA : in std_logic_vector(2 downto 0);
         WD: in std_logic_vector(15 downto 0);
         RD1: out std_logic_vector(15 downto 0);
         RD2: out std_logic_vector(15 downto 0) 
   );
end RF;

architecture Behavioral of RF is
type memory_t is array(0 to 7) of std_logic_vector(15 downto 0);
signal memory: memory_t;

begin

    RD1 <= memory(conv_integer(RA1));
    RD2 <= memory(conv_integer(RA2));
    
    process(clk)
    begin
        if rising_edge(clk) then
            if RegWrite = '1' and enable = '1' then
                memory(conv_integer(WA)) <= WD;
            end if;
        end if;
    end process;

end Behavioral;
