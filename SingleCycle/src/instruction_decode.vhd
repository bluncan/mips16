----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/01/2020 05:22:28 PM
-- Design Name: 
-- Module Name: instruction_decode - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity instruction_decode is
  Port ( clk: in std_logic;
         enable: in std_logic;
         RegWrite: in std_logic;
         Instr: in std_logic_vector(12 downto 0);
         RegDst: in std_logic;
         ExtOp: in std_logic;
         WD: in std_logic_vector(15 downto 0);
         
         RD1: out std_logic_vector(15 downto 0);
         RD2: out std_logic_vector(15 downto 0);
         Ext_Imm: out std_logic_vector(15 downto 0);
         func: out std_logic_vector(2 downto 0);
         sa: out std_logic
   );
end instruction_decode;

architecture Behavioral of instruction_decode is
component RF is
  Port (
         clk: in std_logic;
         enable: in std_logic;
         RA1: in std_logic_vector(2 downto 0);
         RA2: in std_logic_vector(2 downto 0);
         RegWrite: in std_logic;
         WA : in std_logic_vector(2 downto 0);
         WD: in std_logic_vector(15 downto 0);
         RD1: out std_logic_vector(15 downto 0);
         RD2: out std_logic_vector(15 downto 0) 
   );
end component;

signal WA: std_logic_vector(2 downto 0) := "000";

begin

register_file: RF port map (clk=>clk, enable=>enable, RA1=>Instr(12 downto 10), RA2=>Instr(9 downto 7), RegWrite=>RegWrite, WA=>WA, WD=>WD, RD1=>RD1, RD2=>RD2); 

WA <= Instr(9 downto 7) when RegDst = '0' else Instr(6 downto 4);
func <= Instr(2 downto 0);
sa <= Instr(3);
Ext_Imm <= "000000000" & Instr(6 downto 0) when ExtOp = '0' else Instr(6)&Instr(6)&Instr(6)&Instr(6)&Instr(6)&Instr(6)&Instr(6)&Instr(6)&Instr(6)&Instr(6 downto 0); 

end Behavioral;
