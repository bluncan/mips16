----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/08/2020 06:30:34 PM
-- Design Name: 
-- Module Name: ex - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ex is
    Port( RD1: in std_logic_vector(15 downto 0);
          ALUSrc: in std_logic;
          RD2: in std_logic_vector(15 downto 0);
          Ext_Imm: in std_logic_vector(15 downto 0);
          sa: std_logic;
          func: in std_logic_vector(2 downto 0);
          AluOp: in std_logic_vector(2 downto 0);
          Pc_Plus: in std_logic_vector(15 downto 0);
          
          Zero: out std_logic;
          ALUOut: out std_logic_vector(15 downto 0);
          BranchAddress: out std_logic_vector(15 downto 0);
          JumpR: out std_logic  
     );
end ex;

architecture Behavioral of ex is
signal ALUCtrl: std_logic_vector(2 downto 0);
signal mux1_q: std_logic_vector(15 downto 0);
signal A: std_logic_vector(15 downto 0);
signal B: std_logic_vector(15 downto 0);
signal ALURes: std_logic_vector(15 downto 0);
begin
    ALUCtrl_process: process(ALUOp, func)
    begin
        JumpR <= '0';
        case ALUOp is
        when "000" =>
            case func is
                when "000" => ALUCtrl <= "000"; 
                when "001" => ALUCtrl <= "001"; 
                when "010" => ALUCtrl <= "010"; 
                when "011" => ALUCtrl <= "011"; 
                when "100" => ALUCtrl <= "100"; 
                when "101" => ALUCtrl <= "101"; 
                when "110" => ALUCtrl <= "000"; JumpR <= '1';
                when "111" => ALUCtrl <= "110"; 
           end case; 
        when "001" => ALUCtrl <= "000"; 
        when "010" => ALUCtrl <= "001"; 
        when "011" => ALUCtrl <= "111";
        when "100" => ALUCtrl <= "101"; 
        when others => ALUCtrl <= "XXX"; JumpR <= 'X';
    end case;
    end process;
    
    
    A <= RD1;
    B <= RD2 when ALUSrc = '0' else Ext_Imm;
    Zero <= '1' when ALURes = X"0000" else '0';
    ALUOut <= ALURes;
    ALU_process: process(ALUCtrl, A, B, sa)
    begin
        case ALUCtrl is
            when "000" => ALURes <= A + B;
            when "001" => ALURes <= A - B;
            when "010" => 
                if sa = '1' then
                    ALURes <= B(14 downto 0) & '0';
                else
                    ALURes <= B;
                end if;
           when "011" =>
                if sa = '1' then
                    ALURes <= '0' & B(15 downto 1);
                else
                    ALURes <= B;
                end if;
           when "100" => ALURes <= A and B;
           when "101" => ALURes <= A or B;
           when "110" => 
                case A is
                    when X"0000" => ALURes <= B;
                    when X"0001" => ALURes <= B(14 downto 0) & '0';
                    when X"0002" => ALURes <= B(13 downto 0) & "00";
                    when X"0003" => ALURes <= B(12 downto 0) & "000";
                    when X"0004" => ALURes <= B(11 downto 0) & "0000";
                    when X"0005" => ALURes <= B(10 downto 0) & "00000";
                    when X"0006" => ALURes <= B(9 downto 0) &  "000000";
                    when X"0007" => ALURes <= B(8 downto 0) & "0000000";
                    when X"0008" => ALURes <= B(7 downto 0) & "00000000";
                    when X"0009" => ALURes <= B(6 downto 0) & "000000000";
                    when X"000A" => ALURes <= B(5 downto 0) & "0000000000";
                    when X"000B" => ALURes <= B(4 downto 0) & "00000000000";
                    when X"000C" => ALURes <= B(3 downto 0) & "000000000000";
                    when X"000D" => ALURes <= B(2 downto 0) & "0000000000000";
                    when X"000E" => ALURes <= B(1 downto 0) & "00000000000000";
                    when X"000F" => ALURes <= B(0) & "000000000000000";
                    when others => ALURes <= X"0000";
                end case;
           when "111" => 
               if signed(A) < signed(B) then
                   ALURes <= X"0001";
               else
                   ALURes <= X"0000";
               end if;
           end case;
    end process;
    
BranchAddress <= PC_Plus + Ext_Imm;

end Behavioral;
